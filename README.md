# LinkBot
[![Build Status](https://www.gitlab.com/coolbots7/linkbot/badges/master/build.svg)](https://www.gitlab.com/coolbots7/linkbot)

[Invite](https://discordapp.com/api/oauth2/authorize?client_id=544368825387188276&permissions=67584&scope=bot "Invite LinkBot to your server")
---

LinkBot is a discord bot that can lookup song links from one streaming platorm on others.

Currently the streaming services supported are:
* Soundcloud
* Youtube

Planned streaming services:
* Spotify
* Google Play Music



## Usage

| Command | Description |
| ------ | ------|
| !link&#160;&lt;URL&gt; | LinkBot will respond with a message containing links to the song on Youtube and Soundcloud |

### Example
![example](./example.png)

## Installation

### Docker Run
```
$ docker run -e "DiscordToken=<Discord Bot Token>" \
-e "YoutubeToken=<Youtube Token>" \
-e "SoundcloudToken=<Soundcloud Token>" \
registry.gitlab.com/coolbots7/linkbot:latest
```

### Docker Compose
docker-compose.yml
```
version: "3"
services:
  linkbot:
    image: registry.gitlab.com/coolbots7/linkbot:latest
    environemnt:
     - "DiscordToken=<Discord Bot Token>"
     - "YoutubeToken=<Youtube Token>"
     - "SoundcloudToken=<Soundcloud Token>"
```

### Kubernetes
linkbot-deploy.yaml
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: linkbot
spec:
  selector:
    matchLabels:
      app: linkbot
  replicas: 1
  template:
    metadata:
      labels:
        app: linkbot
    spec:
      containers:
      - name: linkbot
        image: registry.gitlab.com/coolbots7/linkbot:latest
        env:
        - name: DiscordToken
          value: "<Discord Token>"
        - name: YoutubeToken
          value: "<Youtube Token>"
        - name: SoundcloudToken
          value: "<Soundcloud Token>"
```
