var Discord = require('discord.io');
var logger = require('winston');
var auth = {}
auth.token = process.env.DiscordToken;


// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
     colorize: true
});
logger.level = 'debug';

// Initialize Discord Bot
var bot = new Discord.Client({
     token: auth.token,
     autorun: true
});

bot.on('ready', function(evt) {
     logger.info('Connected');
     logger.info('Logged in as: ');
     logger.info(bot.username + ' - (' + bot.id + ')');
     for (var serverID in bot.servers) {
          if (bot.servers.hasOwnProperty(serverID)) {
               var server = bot.servers[serverID];
               logger.info("Joined Server on startup: " + server.name + " (" + serverID + ") in " + server.region);
          }
     }
});
bot.on('message', function(user, userID, channelID, message, evt) {
     // Our bot needs to know if it will execute a command
     // It will listen for messages that will start with `!`

     //clean message by replacing multiple spaces with a single space and trim the ends
     message = message.trim().replace(/\s\s+/g, ' ');

     if (message.substring(0, 1) == '!') {
          var args = message.substring(1).split(' ');
          var cmd = args[0];

          args = args.splice(1);
          switch (cmd.toLowerCase()) {
               // !link
               case 'translate':
                    logger.info('Got command "!translate". Parsing message: "' + message + '"');
                    bot.simulateTyping(channelID, function(err, response) {});
                    break;
                    // Just add any case commands if you want to..
          }
     }
});

bot.on('any', function(evt) {
     switch (evt.t) {
          case 'GUILD_CREATE':
               logger.info("Joined Server: " + evt.d.name + " (" + evt.d.id + ") in " + evt.d.region);
               break;
          case 'GUILD_DELETE':
               logger.info("Left Server: " + evt.d.name + " (" + evt.d.id + ") in " + evt.d.region);
               break;
     }
});
